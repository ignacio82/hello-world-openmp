program test
	use omp_lib
	integer	:: thread_num
	! Specify number of threads to use:
	!$ call omp_set_num_threads(8)
	print *, "Testing openmp ..."

	!$omp parallel private(thread_num)
	!$ thread_num = omp_get_thread_num()
	!$omp critical
	!$ print *, "This is thread = ", thread_num
	!$omp end critical
	!$omp end parallel
end program test
