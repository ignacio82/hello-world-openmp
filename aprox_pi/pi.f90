program test
	use omp_lib
	integer				:: n, i
	double precision	:: dx, pisum, x, pi

	n = 1000
	dx = 1.0d0/n
	pisum = 0.0d0 
	! Specify number of threads to use:
	!$ call omp_set_num_threads(2)
	!$omp parallel do reduction(+: pisum) &
	!$omp					private(x)
	do i=1,n
		x=(i-0.d0)*dx
		pisum = pisum + 1.0d0 / (1.0d0+x**2)
	end do
	!$omp end parallel do
	pi = 4.0d0 * dx * pisum
	print *, pi
end program test
