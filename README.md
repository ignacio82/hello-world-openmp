#This is a simple OpenMP hello world Fortran program
##To get this code and run it just do the following:
```BASH
mkdir bitbucket
cd bitbucket
git clone git@bitbucket.org:ignacio82/hello-world-openmp.git
cd hello-world-openmp
make
./openMP_test.X
```
